{
    "id": "385ad352-1046-403b-aded-c6ab379bb207",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRulesMsg",
    "eventList": [
        {
            "id": "4e001192-a0e7-4eda-989d-f1777b195279",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "385ad352-1046-403b-aded-c6ab379bb207"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c63e02a3-74a7-40ea-b6a5-d1a87f81baae",
    "visible": true
}