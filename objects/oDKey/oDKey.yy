{
    "id": "4e166186-5759-4ca0-bc20-ea80eb7db86c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDKey",
    "eventList": [
        {
            "id": "afdf57d7-59ee-48bb-a607-63095f12c318",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4e166186-5759-4ca0-bc20-ea80eb7db86c"
        },
        {
            "id": "969cc519-9425-4a8b-bcc6-4ae594983209",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4e166186-5759-4ca0-bc20-ea80eb7db86c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "676cc0d3-b321-4dbb-aea3-c273d16bcad7",
    "visible": true
}