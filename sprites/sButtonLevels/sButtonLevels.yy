{
    "id": "6992d55c-9e31-4b07-ba6b-16d085962fc8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonLevels",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab591b3d-90f6-4531-a46a-1acc6feb5d4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6992d55c-9e31-4b07-ba6b-16d085962fc8",
            "compositeImage": {
                "id": "2ce01511-0d0b-42c3-acd2-2de4ec0cbc0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab591b3d-90f6-4531-a46a-1acc6feb5d4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d46f31ad-fcf7-439d-bcd0-41d4cf8046a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab591b3d-90f6-4531-a46a-1acc6feb5d4e",
                    "LayerId": "6c23ac1e-ccbb-4bbc-b7f0-36e5921ee09a"
                }
            ]
        },
        {
            "id": "95dd33fd-f2ab-4ca1-a05e-80b084d9fda1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6992d55c-9e31-4b07-ba6b-16d085962fc8",
            "compositeImage": {
                "id": "3297aba6-269b-4452-abda-84852449bee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95dd33fd-f2ab-4ca1-a05e-80b084d9fda1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b695a04-ba9d-44e3-b733-ba85e293335d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95dd33fd-f2ab-4ca1-a05e-80b084d9fda1",
                    "LayerId": "6c23ac1e-ccbb-4bbc-b7f0-36e5921ee09a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6c23ac1e-ccbb-4bbc-b7f0-36e5921ee09a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6992d55c-9e31-4b07-ba6b-16d085962fc8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 53,
    "yorig": 13
}