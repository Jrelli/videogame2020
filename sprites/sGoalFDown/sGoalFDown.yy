{
    "id": "36146982-658d-4d66-b0f5-868c3b2560eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGoalFDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 1,
    "bbox_right": 445,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45ed9367-dfe1-4914-9609-d6e5dbd13213",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36146982-658d-4d66-b0f5-868c3b2560eb",
            "compositeImage": {
                "id": "8c4a54ba-2c00-4e26-9ffc-42b22ae3c77b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45ed9367-dfe1-4914-9609-d6e5dbd13213",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8716adc3-9a5b-4459-9cc9-639795a38e99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45ed9367-dfe1-4914-9609-d6e5dbd13213",
                    "LayerId": "6bea71f8-c576-40f9-a8ea-00cd9fdbdf1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 97,
    "layers": [
        {
            "id": "6bea71f8-c576-40f9-a8ea-00cd9fdbdf1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36146982-658d-4d66-b0f5-868c3b2560eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 445,
    "xorig": 222,
    "yorig": 48
}