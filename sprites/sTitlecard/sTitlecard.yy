{
    "id": "3b8b3250-14e2-41a1-9c81-bdb7682b667c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitlecard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 347,
    "bbox_left": 8,
    "bbox_right": 775,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4471b0b9-baa2-4804-b8b0-8f3fe46ad8db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b8b3250-14e2-41a1-9c81-bdb7682b667c",
            "compositeImage": {
                "id": "b1f7a322-b553-4d5c-824c-edefd83c1814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4471b0b9-baa2-4804-b8b0-8f3fe46ad8db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b24b3495-b7f6-44f5-8961-c9e92a6cf02c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4471b0b9-baa2-4804-b8b0-8f3fe46ad8db",
                    "LayerId": "b32f6263-f024-49d9-b97c-65463cc6d020"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "b32f6263-f024-49d9-b97c-65463cc6d020",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b8b3250-14e2-41a1-9c81-bdb7682b667c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 400,
    "yorig": 200
}