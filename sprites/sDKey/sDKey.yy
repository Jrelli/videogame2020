{
    "id": "676cc0d3-b321-4dbb-aea3-c273d16bcad7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 76,
    "bbox_top": 64,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d6159dd-85ce-4d97-bec7-9721c5b8199e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "676cc0d3-b321-4dbb-aea3-c273d16bcad7",
            "compositeImage": {
                "id": "3e9ef768-74b7-47a8-9a72-8d6dece87091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d6159dd-85ce-4d97-bec7-9721c5b8199e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bb32f5a-77f6-46b5-ab46-97acac4a7e95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d6159dd-85ce-4d97-bec7-9721c5b8199e",
                    "LayerId": "e1d3a6ff-2d51-4f99-ae09-40cf3b4ed070"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "e1d3a6ff-2d51-4f99-ae09-40cf3b4ed070",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "676cc0d3-b321-4dbb-aea3-c273d16bcad7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 38,
    "yorig": 38
}