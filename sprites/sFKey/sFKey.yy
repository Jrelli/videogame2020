{
    "id": "ed609840-1733-41a4-aceb-686b8faf0d78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 76,
    "bbox_top": 62,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b112b29b-b279-4957-adaf-da127a430433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed609840-1733-41a4-aceb-686b8faf0d78",
            "compositeImage": {
                "id": "c77bc92f-7a50-43b3-ae98-3cd9626b41f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b112b29b-b279-4957-adaf-da127a430433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4a6f3f9-2dfb-416b-92a5-e40d4900f121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b112b29b-b279-4957-adaf-da127a430433",
                    "LayerId": "80e202af-10f4-4933-bfb0-653668a75fb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "80e202af-10f4-4933-bfb0-653668a75fb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed609840-1733-41a4-aceb-686b8faf0d78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 38,
    "yorig": 38
}