{
    "id": "c63e02a3-74a7-40ea-b6a5-d1a87f81baae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRulesMsg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f607975f-0ca4-45c5-bad9-68bcd0a5a845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63e02a3-74a7-40ea-b6a5-d1a87f81baae",
            "compositeImage": {
                "id": "e3da302a-3f52-4b52-a350-54281997d516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f607975f-0ca4-45c5-bad9-68bcd0a5a845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5736087e-d49d-4b43-9638-3602a0ef217c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f607975f-0ca4-45c5-bad9-68bcd0a5a845",
                    "LayerId": "f44cfd82-bf96-4f5a-8590-5e944b046e3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "f44cfd82-bf96-4f5a-8590-5e944b046e3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c63e02a3-74a7-40ea-b6a5-d1a87f81baae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}