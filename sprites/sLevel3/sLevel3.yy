{
    "id": "b27668cb-1af7-4338-aa32-e57ca6d9b98e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLevel3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 169,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2f4b61b-7604-4994-aa61-12385d4b7217",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b27668cb-1af7-4338-aa32-e57ca6d9b98e",
            "compositeImage": {
                "id": "1fdc26ef-b9fb-4193-b5f8-810060bf53ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2f4b61b-7604-4994-aa61-12385d4b7217",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d257d0d-02a0-443f-8a5f-24963e3a0860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2f4b61b-7604-4994-aa61-12385d4b7217",
                    "LayerId": "25ae65eb-1ebf-4d1e-b78a-afa3b527ec50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 170,
    "layers": [
        {
            "id": "25ae65eb-1ebf-4d1e-b78a-afa3b527ec50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b27668cb-1af7-4338-aa32-e57ca6d9b98e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 85
}