{
    "id": "6414925d-23c0-4fed-acad-1671a2f1151e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "buttonTemplate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 34,
    "bbox_right": 167,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03ffc189-ca06-49eb-b484-8e2a41270578",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6414925d-23c0-4fed-acad-1671a2f1151e",
            "compositeImage": {
                "id": "f225b77e-3141-44d1-ba2a-7a67b3a3c238",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03ffc189-ca06-49eb-b484-8e2a41270578",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42216ba2-cd13-41e4-bf09-70bfac86f3e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03ffc189-ca06-49eb-b484-8e2a41270578",
                    "LayerId": "933eb3ea-2ffc-4159-82fd-c7675bfd62fc"
                }
            ]
        },
        {
            "id": "7f8a93ef-ff8b-445a-a3e7-ff3ca18b217e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6414925d-23c0-4fed-acad-1671a2f1151e",
            "compositeImage": {
                "id": "e45e5a31-dadd-4be9-ab22-972b3ac43f43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f8a93ef-ff8b-445a-a3e7-ff3ca18b217e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc8879b2-ab37-48d4-a1c4-79db39ea94ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f8a93ef-ff8b-445a-a3e7-ff3ca18b217e",
                    "LayerId": "933eb3ea-2ffc-4159-82fd-c7675bfd62fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "933eb3ea-2ffc-4159-82fd-c7675bfd62fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6414925d-23c0-4fed-acad-1671a2f1151e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}