{
    "id": "c1df55b4-07ad-4034-889a-94952c6b3131",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDance3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 204,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0b8fc84-51c2-4032-ace3-9c7b1d812fb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1df55b4-07ad-4034-889a-94952c6b3131",
            "compositeImage": {
                "id": "cbbc8fe8-5d9f-4239-981d-69644733d181",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0b8fc84-51c2-4032-ace3-9c7b1d812fb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1d87964-7719-44e9-95d9-5ce61385bed2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0b8fc84-51c2-4032-ace3-9c7b1d812fb1",
                    "LayerId": "6c0f42ae-5889-4565-8b8f-7c1247b26ee3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "6c0f42ae-5889-4565-8b8f-7c1247b26ee3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1df55b4-07ad-4034-889a-94952c6b3131",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 102,
    "yorig": 150
}