{
    "id": "d5983170-c29d-475e-acda-5765a83ab790",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDance2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 204,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3ebd3fa-afbd-4a14-a579-fedbcbd16af3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5983170-c29d-475e-acda-5765a83ab790",
            "compositeImage": {
                "id": "10c85669-8ae5-4381-a29e-7d074e325a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3ebd3fa-afbd-4a14-a579-fedbcbd16af3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f984e9f-a645-479d-89d5-53f9fbb60ce5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3ebd3fa-afbd-4a14-a579-fedbcbd16af3",
                    "LayerId": "091ed260-0e41-4b6b-8047-9e2f77de1bf6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "091ed260-0e41-4b6b-8047-9e2f77de1bf6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5983170-c29d-475e-acda-5765a83ab790",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 102,
    "yorig": 150
}