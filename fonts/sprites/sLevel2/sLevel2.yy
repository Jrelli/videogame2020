{
    "id": "f0144753-c4f0-49fb-9f15-f63375867f7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLevel2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 169,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3011de08-86df-4a5c-900a-6c4a63a09d79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0144753-c4f0-49fb-9f15-f63375867f7c",
            "compositeImage": {
                "id": "9d53f587-bc33-415b-8e7c-ea92b8972ada",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3011de08-86df-4a5c-900a-6c4a63a09d79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bb46daa-08ca-48f4-9b7a-e344ffce95c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3011de08-86df-4a5c-900a-6c4a63a09d79",
                    "LayerId": "7805610f-b35e-4a22-aa2e-efdd841e93f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 170,
    "layers": [
        {
            "id": "7805610f-b35e-4a22-aa2e-efdd841e93f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0144753-c4f0-49fb-9f15-f63375867f7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 85
}