{
    "id": "6d4bf50e-6152-47cf-8173-8ae3ea06361b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLogo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 449,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d21eb5f-cf71-44d4-8b0b-47e69df75a75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d4bf50e-6152-47cf-8173-8ae3ea06361b",
            "compositeImage": {
                "id": "b12d2496-b320-4899-a6fe-f37af9a3cf20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d21eb5f-cf71-44d4-8b0b-47e69df75a75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94d0c637-9231-4e6d-9fa8-4a5ed48c3030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d21eb5f-cf71-44d4-8b0b-47e69df75a75",
                    "LayerId": "178e6402-917b-4a7a-9f1b-9f0ff6ef732b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 450,
    "layers": [
        {
            "id": "178e6402-917b-4a7a-9f1b-9f0ff6ef732b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d4bf50e-6152-47cf-8173-8ae3ea06361b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 300,
    "yorig": 225
}