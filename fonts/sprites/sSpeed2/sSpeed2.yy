{
    "id": "aae4d7e1-5663-48be-b353-9f8c837177cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpeed2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a857f83-439b-48fb-ae61-f8917b5692fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aae4d7e1-5663-48be-b353-9f8c837177cc",
            "compositeImage": {
                "id": "baf4e624-8b53-47b7-bdfd-a2f24376faef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a857f83-439b-48fb-ae61-f8917b5692fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f68075bd-1deb-44de-9f11-bf6d8f1ff682",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a857f83-439b-48fb-ae61-f8917b5692fa",
                    "LayerId": "bf1d6a9d-c271-4bb0-95cc-0ddac942bf52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "bf1d6a9d-c271-4bb0-95cc-0ddac942bf52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aae4d7e1-5663-48be-b353-9f8c837177cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 50
}