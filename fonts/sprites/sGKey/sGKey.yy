{
    "id": "41b1b519-b4ce-4e2e-b2f8-e4f712c3be9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 76,
    "bbox_top": 66,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61e7134e-4ec8-4dba-9729-d439cb35a1d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41b1b519-b4ce-4e2e-b2f8-e4f712c3be9e",
            "compositeImage": {
                "id": "aa76e8cd-8942-4c04-b47c-ea78d8ba15c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61e7134e-4ec8-4dba-9729-d439cb35a1d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1fa9c91-9bbe-4b6e-8242-472927ed9174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61e7134e-4ec8-4dba-9729-d439cb35a1d2",
                    "LayerId": "24dbc4a0-cbe6-493b-a05d-c98427a94b00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "24dbc4a0-cbe6-493b-a05d-c98427a94b00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41b1b519-b4ce-4e2e-b2f8-e4f712c3be9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 38,
    "yorig": 38
}