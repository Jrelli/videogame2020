{
    "id": "75fdb969-b704-411d-9325-13f3c649e0c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGoalGDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 1,
    "bbox_right": 445,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8b130a7-7878-4393-b858-91061c266eb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75fdb969-b704-411d-9325-13f3c649e0c7",
            "compositeImage": {
                "id": "847323e9-2eff-4ee8-b8da-299c95075ed5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8b130a7-7878-4393-b858-91061c266eb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac5bed43-4f84-4da5-b22e-0956173d88fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8b130a7-7878-4393-b858-91061c266eb1",
                    "LayerId": "aa62b27a-bf70-4ad1-8c1f-340413f3de72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 97,
    "layers": [
        {
            "id": "aa62b27a-bf70-4ad1-8c1f-340413f3de72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75fdb969-b704-411d-9325-13f3c649e0c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 445,
    "xorig": 222,
    "yorig": 48
}