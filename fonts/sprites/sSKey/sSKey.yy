{
    "id": "a86951bf-5007-4c7f-bbdc-8f88795e972c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 76,
    "bbox_top": 66,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ebec4cd-913a-44ac-90b1-3bcfebdd1d30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a86951bf-5007-4c7f-bbdc-8f88795e972c",
            "compositeImage": {
                "id": "0c2ee05e-b806-4bf6-8a1a-29338c8696d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ebec4cd-913a-44ac-90b1-3bcfebdd1d30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f9cf4b-2da9-46cd-812f-20433f4c6c45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ebec4cd-913a-44ac-90b1-3bcfebdd1d30",
                    "LayerId": "850a4a89-f98a-46fe-9e63-4af78e997241"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "850a4a89-f98a-46fe-9e63-4af78e997241",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a86951bf-5007-4c7f-bbdc-8f88795e972c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 38,
    "yorig": 38
}