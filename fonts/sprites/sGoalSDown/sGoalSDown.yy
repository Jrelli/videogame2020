{
    "id": "a9a7a097-fd23-408e-a3f5-fd8bf5650594",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGoalSDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 1,
    "bbox_right": 445,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb2bd694-756e-49fd-acbd-5bb800074d97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9a7a097-fd23-408e-a3f5-fd8bf5650594",
            "compositeImage": {
                "id": "3dbe79c4-2837-40b6-83a3-dcae5c520530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb2bd694-756e-49fd-acbd-5bb800074d97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb662d8a-72c3-47ff-92b1-7dc28f486f71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb2bd694-756e-49fd-acbd-5bb800074d97",
                    "LayerId": "fdda79b6-f4d7-4d6c-8414-59d9a67365f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 97,
    "layers": [
        {
            "id": "fdda79b6-f4d7-4d6c-8414-59d9a67365f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9a7a097-fd23-408e-a3f5-fd8bf5650594",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 445,
    "xorig": 222,
    "yorig": 48
}