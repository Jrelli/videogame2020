{
    "id": "61468be4-8d96-4595-8737-dd98fa8ac50f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTitlecard",
    "eventList": [
        {
            "id": "e6219693-aa92-419a-b27e-0b0dbded5691",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "61468be4-8d96-4595-8737-dd98fa8ac50f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b8b3250-14e2-41a1-9c81-bdb7682b667c",
    "visible": true
}