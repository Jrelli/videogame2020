{
    "id": "35bee4a0-5d79-4621-8aea-b35d2f005652",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAKey",
    "eventList": [
        {
            "id": "00c3f639-9dfb-4f2c-820e-fdba537e7ba5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "35bee4a0-5d79-4621-8aea-b35d2f005652"
        },
        {
            "id": "eae43ac3-043d-40be-9e28-c2f0d894ad46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "35bee4a0-5d79-4621-8aea-b35d2f005652"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c5509359-2569-4d75-a458-4a7d06744e02",
    "visible": true
}