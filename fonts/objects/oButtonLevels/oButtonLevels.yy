{
    "id": "85ba967d-2c55-40f2-98c1-3df393d54ba2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonLevels",
    "eventList": [
        {
            "id": "4d0516e9-9998-4993-8a30-506c800d78b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "85ba967d-2c55-40f2-98c1-3df393d54ba2"
        },
        {
            "id": "ae8140f9-618b-44f4-896a-2385a41ec55e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "85ba967d-2c55-40f2-98c1-3df393d54ba2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6992d55c-9e31-4b07-ba6b-16d085962fc8",
    "visible": true
}