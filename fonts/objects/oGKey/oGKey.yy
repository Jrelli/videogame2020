{
    "id": "9a2086c2-5315-43eb-9984-67fef3f42279",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGKey",
    "eventList": [
        {
            "id": "365a4160-0112-4d79-96ea-9ad3329526ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9a2086c2-5315-43eb-9984-67fef3f42279"
        },
        {
            "id": "b48b88f3-8469-407a-ad7d-ff9e9cda3490",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9a2086c2-5315-43eb-9984-67fef3f42279"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "41b1b519-b4ce-4e2e-b2f8-e4f712c3be9e",
    "visible": true
}