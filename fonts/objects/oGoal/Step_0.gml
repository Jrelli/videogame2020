//send targets
tarCool--;
if(tarCool <= 0){
	chosTar= irandom_range(1, global.maxTar);
	if(chosTar == 1){
		instance_create_layer(x, y, "Targets", oAKey);
	}else if(chosTar == 2){
		instance_create_layer(x, y, "Targets", oSKey);
	}else if(chosTar == 3){
		instance_create_layer(x, y, "Targets", oDKey);
	}else if(chosTar == 4){
		instance_create_layer(x, y, "Targets", oFKey);
	}else if(chosTar == 5){
		instance_create_layer(x, y, "Targets", oGKey);
	}
	tarCool = global.tarCool_Lock;
}

//highlight goal
if keyboard_check (vk_left){
	sprite_index = sGoalADown;
}else if keyboard_check (vk_down){
	sprite_index = sGoalSDown;
}else if keyboard_check (vk_right){
	sprite_index = sGoalDDown;
}else if keyboard_check (vk_up){
	sprite_index = sGoalFDown;
}else if keyboard_check (vk_enter){
	sprite_index = sGoalGDown;
}else{
	sprite_index = sGoal;
}

//misses target
if (instance_exists(oAKey)){
	if (instance_nearest(x, room_height, oAKey).y > room_height){
		instance_destroy(instance_nearest(x, room_height, oAKey));
		with (oScore) thescore = thescore - 5;
		with (oBaseDance) sprite_index = sBaseDance;
	}
}
if (instance_exists(oSKey)){
	if (instance_nearest(x, room_height, oSKey).y > room_height){
		instance_destroy(instance_nearest(x, room_height, oSKey));
		with (oScore) thescore = thescore - 5;
		with (oBaseDance) sprite_index = sBaseDance;
	}
}
if (instance_exists(oDKey)){
	if (instance_nearest(x, room_height, oDKey).y > room_height){
		instance_destroy(instance_nearest(x, room_height, oDKey));
		with (oScore) thescore = thescore - 5;
		with (oBaseDance) sprite_index = sBaseDance;
	}
}
if (instance_exists(oFKey)){
	if (instance_nearest(x, room_height, oFKey).y > room_height){
		instance_destroy(instance_nearest(x, room_height, oFKey));
		with (oScore) thescore = thescore - 5;
		with (oBaseDance) sprite_index = sBaseDance;
	}
}
if (instance_exists(oGKey)){
	if (instance_nearest(x, room_height, oGKey).y > room_height){
		instance_destroy(instance_nearest(x, room_height, oGKey));
		with (oScore) thescore = thescore - 5;
		with (oBaseDance) sprite_index = sBaseDance;
	}
}