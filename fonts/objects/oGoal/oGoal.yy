{
    "id": "aff08dbf-08d4-4d3b-9fe1-58b79ba83271",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGoal",
    "eventList": [
        {
            "id": "f689b429-9c83-4d74-846c-4613b9f2b54a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aff08dbf-08d4-4d3b-9fe1-58b79ba83271"
        },
        {
            "id": "da32672e-63d7-412f-b369-a3a7f69b484d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aff08dbf-08d4-4d3b-9fe1-58b79ba83271"
        },
        {
            "id": "5a93bd75-9a81-4b9f-96e7-c6d1e9df3031",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "35bee4a0-5d79-4621-8aea-b35d2f005652",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "aff08dbf-08d4-4d3b-9fe1-58b79ba83271"
        },
        {
            "id": "c3e5b239-e8ac-42f4-9562-3e7d5b9e9fa1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a3261ea3-c60b-4856-9f23-a8315f9071ce",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "aff08dbf-08d4-4d3b-9fe1-58b79ba83271"
        },
        {
            "id": "d4683a5e-3c72-4162-8e55-c1e083cbbc6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4e166186-5759-4ca0-bc20-ea80eb7db86c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "aff08dbf-08d4-4d3b-9fe1-58b79ba83271"
        },
        {
            "id": "629a59c6-5c4b-4ad2-be0e-5813c611e568",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9a6679f4-e562-43da-8ff4-2f5a28023240",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "aff08dbf-08d4-4d3b-9fe1-58b79ba83271"
        },
        {
            "id": "5a230111-0981-4e26-928b-b8f9d14d05bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9a2086c2-5315-43eb-9984-67fef3f42279",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "aff08dbf-08d4-4d3b-9fe1-58b79ba83271"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aa0a1a2b-2734-43cb-af91-bab6a2f48f7e",
    "visible": true
}