if (keyboard_check(vk_right))||(keyboard_check(vk_up))||(keyboard_check(vk_down))||(keyboard_check(vk_enter)){
	if (keyboard_check(vk_left)){
		if (mouse_check_button_pressed(mb_left) || keyboard_check_pressed(vk_space)){
			with (oScore) thescore = thescore - 10;
			with (oBaseDance) sprite_index = sBaseDance;
		}
	}
}

if (keyboard_check(vk_right)){
	if (mouse_check_button_pressed(mb_left) || keyboard_check_pressed(vk_space)){
		with instance_nearest(x, y, oDKey) instance_destroy();
		with (oScore) thescore = thescore + 5;
		with (oBaseDance) sprite_index = sDance3;
	}
}
